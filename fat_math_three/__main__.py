# Default imports
import sys

# Pygame imports
import pygame

from pygame import display, event

# Local imports
from config import Screen
from data import init_fonts, init_sounds, init_songs, play_music
from room import init_rooms
from intro import play_intro

from gameevents import CUSTOM_EVENTS

from engine import Engine

def init():
    pygame.init()
    init_fonts()
    init_sounds()
    init_songs()
    init_rooms()
    display.set_caption('Fat Math Three')
    display.set_mode(Screen.Size)

    # Block all events except QUIT and KEYDOWN
    event.set_blocked(None)
    event.set_allowed([pygame.QUIT, pygame.KEYDOWN] + CUSTOM_EVENTS)

def main():
    init()
    play_intro()
    play_music('calm')
    Engine.init()
    Engine.enter_room(name='intro')
    Engine.run()
