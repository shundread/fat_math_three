'''This file defines a Sprite class, which is used to memorize the latest
drawable state of the image.'''

from random import randint

class Sprite(object):
    Decay = 300.0

    def __init__(self, position):
        self.position = position
        self.__dst = None
        self.__image = None
        self.__shake_duration = 0
        self.__shake_intensity = 0

    def set_drift(self, position):
        self.__dst = position

    def set_shake(self, duration, intensity):
        self.__shake_duration = duration
        self.__shake_intensity = intensity

    def render(self):
        raise NotImplementedError

    def draw(self, surface, offset=(0, 0)):
        if self.__image == None:
            self.__image = self.render()

        (w, h) = self.__image.get_size()

        # Add shake
        if self.__shake_duration > 0:
            sx = randint(-self.__shake_intensity, self.__shake_intensity)
            sy = randint(-self.__shake_intensity, self.__shake_intensity)
            offset = (offset[0] + sx, offset[1] + sy)

        pos = (int(self.position[0]+offset[0]-w/2), int(self.position[1]+offset[1]-h/2))
        surface.blit(self.__image, pos)

    def request_redraw(self):
        self.__image = None

    def set_alpha(self, alpha):
        if self.__image == None:
            self.__image = self.render()

        self.__image.set_alpha(alpha)

    def update(self, dt):
        self.__shake_duration = max(self.__shake_duration - dt, 0)

        if self.__dst is None:
            return

        # Calculate next position
        (x0, y0) = self.position
        (x1, y1) = self.__dst
        (xd, yd) = (x1-x0, y1-y0)
        x = x0 + dt*xd/self.Decay
        y = y0 + dt*yd/self.Decay

        # If we're not drifting in an axis any longer due to int rounding,
        # ensure we snap that axis to dst coordinates
        if x0-0.5 <= x1 <= x0+0.5:
            x = x1
        if y0-0.5 <= y1 <= y0+0.5:
            y = y1

        self.position = (x, y)
        if self.position == self.__dst:
            self.__dst = None
