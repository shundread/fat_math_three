'''This class defines an inventory for the player.'''

from activator import ListeningActivator
from calculator import Calculator
from data import get_image
from equation import Equation
from equationsprite import ItemStyle
from sprite import Sprite

class Inventory(object):
    items = []
    orientation = None
    YIncrement = 32

    @classmethod
    def set_drift(cls, calculator_position):
        x = calculator_position[0]
        if calculator_position[1] < 400:
            y = 230
            y_increment = cls.YIncrement
        else:
            y = 370
            y_increment = -cls.YIncrement

        for item in cls.items:
            item.set_drift((x, y))
            y += y_increment

    @classmethod
    def draw(cls, surface, offset=(0, 0)):
        for item in cls.items:
            item.draw(surface, offset)

    @classmethod
    def update(cls, dt):
        for item in cls.items:
            item.update(dt)

class Item(Sprite):
    def __init__(self, difficulty, limits, name, position, requires=None):
        Sprite.__init__(self, position)
        self.difficulty = difficulty
        self.limits = limits
        self.name = name
        self.position = position

    def render(self):
        return get_image(self.name)

    def make_equation(self, used_solutions):
        if self.difficulty == 4:
            formula = 'n+(n*x)'
        elif self.difficulty == 3:
            formula = 'n+(n*n)'
        elif self.difficulty == 2:
            formula = 'n+n-x'
        elif self.difficulty == 1:
            formula = 'n+n+x'
        else:
            formula = 'n+n+n'
        # Default: Difficulty 0
        return Equation.create_restricted(self.limits, formula, used_solutions)


class ItemActivator(ListeningActivator):
    FadeLength = 1000

    def __init__(self, item, equation):
        ListeningActivator.__init__(self, item.position, equation,
            ItemStyle(False), ItemStyle(True))
        self.item = item

        # Compute extra image information
        self.glass_image = get_image('glass')
        self.glass_rect = self.glass_image.get_rect()
        self.glass_rect.center = item.position
        self.glass_rect.y -= 30

        # Fadeout information
        self.fade_timer = 0

    def update(self, dt):
        ListeningActivator.update(self, dt)

        if self.status == self.Solved:
            self.fade_timer += dt
            alpha = int(255.0*max(self.FadeLength - self.fade_timer, 0)/self.FadeLength)
            self.sprite.set_alpha(alpha)

    def solve_callback(self):
        Inventory.items.append(self.item)
        position = self.item.position
        self.item.position = (400, -1000)
        self.item.set_drift(position)


    def draw(self, surface, offset=(0, 0)):
        ListeningActivator.draw(self, surface, offset)

        rect = self.glass_rect
        pos = (rect.topleft[0]+offset[0], rect.topleft[1]+offset[1]+int(self.fade_timer*0.8))
        surface.blit(self.glass_image, pos)

