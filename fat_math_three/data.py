'''Simple data loader module.

Loads data files from the "data" directory shipped with a game.

Enhancing this to handle caching etc. is left as an exercise for the reader.

Note that pyglet users should probably just add the data directory to the
pyglet.resource search path.
'''

import os

from pygame.font import Font
from pygame import image
from pygame.mixer import music

data_py = os.path.abspath(os.path.dirname(__file__))
data_dir = os.path.normpath(os.path.join(data_py, '..', 'data'))

fonts = {}
images = {}
sounds = {}
songs = {}

def path(*path):
    '''Determine the path to a file in the data directory.'''
    return os.path.join(data_dir, *path)

def get_image(name, extension="png", alpha=False):
    if name not in images:
        filename = '{0}.{1}'.format(name, extension)
        images[name] = image.load(path('graphics', filename))
        if alpha:
            images[name] = images[name].convert_alpha()
    return images[name]

def init_fonts():
    fonts['calculator'] = Font(path('fonts', 'digitalreadout', 'Digirtu_.ttf'), 30)
    fonts['corpulent'] = Font(path('fonts', 'corpulent', 'ccaps.ttf'), 30)
    fonts['corpulent_shadow'] = Font(path('fonts', 'corpulent', 'ccapshad.ttf'), 30)
    fonts['regular'] = Font(path('fonts', 'cicle', 'CicleGordita.ttf'), 30)
    fonts['squiggly'] = Font(path('fonts', 'nerwus', 'Nerwus.ttf'), 30)

def init_sounds():
    pass

def init_songs():
    songs['calm'] = path('music', 'NWMA_-_04_-_Morning_Tram.ogg')
    songs['combat'] = path('music', 'NWMA_-_05_-_Byte.ogg')

def play_music(name):
    music.load(songs[name])
    music.play(-1)
