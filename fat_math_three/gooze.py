'''This class defines our enemy, the mighty Gooze [sic].'''

# stdlib imports
from random import choice, randint

# Local imports
from activator import ListeningActivator, RacingActivator
from config import Screen
from data import get_image
from equation import Equation
from equationsprite import AttackStyle, TargetStyle
from gameevents import (
    post_attack_blocked,
    post_damaged_monster,
    post_damaged_player,
    post_missed
)
from player import Player
from sprite import Sprite

class Gooze(Sprite):
    # Timers
    AttackWarmup=10000
    AttackCooldown=5000
    DefenseCooldown=500
    DriftCooldown=2500
    DeathWarmup=1000
    BaseHealth=3

    # Status
    Fighting = 1
    Killed   = 2

    def __init__(self, difficulty):
        Sprite.__init__(self, self.make_offscreen_pos())
        self.set_drift(Screen.Center)
        self.difficulty = difficulty
        self.health = self.BaseHealth
        self.attack_timer = 0
        self.defense_timer = 0
        self.drift_timer = 0
        self.death_timer = 0
        self.status = self.Fighting
        self.attack = None
        self.defense = self.create_defense()
        self.broken_defenses = []
        self.broken_attacks = []

    def make_offscreen_pos(self):
        xpos = choice([-2000, 2000])
        ypos = choice([-1000, 0, 1000])
        return (xpos, ypos)

    def create_attack(self):
        if self.health == 0:
            return

        if self.difficulty == 0:
            formula = 'n+n'
        elif self.difficulty == 1:
            formula = 'n-x'
        elif self.difficulty == 2:
            formula = 'n*n'
        else:
            formula = 'n*x'

        if self.defense:
            restricted = [self.defense.equation.solution]
        else:
            restricted = []

        equation = Equation.create_restricted([1, 10], formula, restricted)
        pos = self.make_offscreen_pos()
        attack = AttackActivator(pos, equation, self.AttackWarmup)
        attack.set_drift((500, 100))
        self.attack = attack

    def create_defense(self):
        if self.health == 0:
            self.status = self.Killed
            return

        if self.difficulty == 0:
            formula = 'n+n+n'
        elif self.difficulty == 1:
            formula = 'n-x+n'
        elif self.difficulty == 2:
            formula = 'x*(n+n)'
        else:
            formula = 'x*x'

        if self.attack:
            restricted = [self.attack.equation.solution]
        else:
            restricted = []

        equation = Equation.create_restricted([1, 10], formula, restricted)
        pos = self.make_offscreen_pos()
        defense = TargetActivator(pos, equation)
        defense.set_drift((400, 300))
        self.defense = defense
        
    def render(self):
        return get_image('gooze', alpha=True)

    def draw(self, surface, offset=(0, 0)):
        Sprite.draw(self, surface, offset)

        if self.attack:
            self.attack.draw(surface, offset)

        if self.defense:
            self.defense.draw(surface, offset)

        for a in self.broken_attacks:
            a.draw(surface, offset)

        for d in self.broken_defenses:
            d.draw(surface, offset)

    def update(self, result, dt):
        Sprite.update(self, dt)

        if result is not None:
            missed = True
        else:
            missed = False

        for b in self.broken_attacks + self.broken_defenses:
            b.update(dt)

        if self.attack:
            self.attack.update(dt)
            if self.attack.equation.solution == result:
                self.attack.solve()
                missed = False
        else:
            self.attack_timer += dt
            if self.attack_timer >= self.AttackCooldown:
                self.create_attack()
                self.attack_timer = 0

        if self.defense:
            self.defense.update(dt)
            if self.defense.equation.solution == result:
                self.defense.solve()
                missed = False
        else:
            self.defense_timer += dt
            if self.defense_timer >= self.DefenseCooldown:
                self.create_defense()
                self.defense_timer = 0

        self.drift_timer += dt
        if self.drift_timer >= self.DriftCooldown:
            xd = randint(-100, 100)
            yd = randint(-50, 50)
            self.set_drift((Screen.xcenter + xd, Screen.ycenter + yd))
            self.drift_timer = 0

        if missed:
            post_missed(2000)

    def drop_attack(self):
        if self.attack:
            self.broken_attacks.append(self.attack)
            self.attack = None

    def hurt(self):
        self.set_shake(1000, 10)
        self.drift_timer = 0

        self.health = max(self.health - 1, 0)
        if self.defense:
            self.broken_defenses.append(self.defense)
            self.defense = None

        # Let's just vanish the attack because doing the right thing has become
        # a thing of the past since at some point earlier today and deadline is
        # looming
        if self.health == 0:
            self.attack = None
            self.set_drift((400, 2000))


##################################
# Specialized monster activators #
##################################

class TargetIconSprite(Sprite):
    def render(self):
        return get_image('target')

class TargetActivator(ListeningActivator):
    FadeLength = 1000

    def __init__(self, position, equation):
        ListeningActivator.__init__(self, position, equation,
            TargetStyle(False), TargetStyle(True))

        # Compute extra image information
        self.target_icon = TargetIconSprite(position)
        self.fade_timer = 0

    def update(self, dt):
        ListeningActivator.update(self, dt)
        self.target_icon.update(dt)

        if self.status != self.Ready:
            self.fade_timer += dt
            alpha = int(255.0*max(self.FadeLength - self.fade_timer, 0)/self.FadeLength)
            self.sprite.set_alpha(alpha)

    def set_drift(self, (x, y)):
        self.sprite.set_drift((x, y))
        self.target_icon.set_drift((x, y-40))

    def solve_callback(self):
        self.target_icon.set_drift((400, 1000))
        post_damaged_monster()

    def draw(self, surface, offset=(0, 0)):
        ListeningActivator.draw(self, surface, offset)
        self.target_icon.draw(surface, offset)

class AttackIconSprite(Sprite):
    def render(self):
        return get_image('fireball')

class AttackActivator(RacingActivator):
    FadeLength = 1000

    def __init__(self, position, equation, time_left):
        RacingActivator.__init__(self, position, equation, time_left,
            AttackStyle(AttackStyle.Ready), AttackStyle(AttackStyle.Solved),
            AttackStyle(AttackStyle.TimedOut))

        # Compute extra image information
        self.fireball_icon = AttackIconSprite(position)
        self.fade_timer = 0

    def update(self, dt):
        RacingActivator.update(self, dt)
        self.fireball_icon.update(dt)

        if self.status != self.Ready:
            self.fade_timer += dt
            alpha = int(255.0*max(self.FadeLength - self.fade_timer, 0)/self.FadeLength)
            self.sprite.set_alpha(alpha)

    def set_drift(self, (x, y)):
        self.sprite.set_drift((x, y))
        self.fireball_icon.set_drift((x, y-40))

    def solve_callback(self):
        self.fireball_icon.set_drift((400, 1000))
        post_attack_blocked()

    def timeout_callback(self):
        self.fireball_icon.set_drift(Player.hearts[-1].position)
        post_damaged_player()

    def draw(self, surface, offset=(0, 0)):
        RacingActivator.draw(self, surface, offset)
        self.fireball_icon.draw(surface, offset)
