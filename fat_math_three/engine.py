# Default imports
from random import randint

# Pygame imports
import pygame
from pygame import display, event, time

# Local imports
from config import FPS

from gameevents import *

from calculator import Calculator
from data import play_music
from inventory import Inventory
from gooze import Gooze
from room import rooms
from quitter import check_quit
from player import Player

class Engine(object):
    EncounterChanceIncrement = 5
    EncounterBreak = 2

    room = None
    calculator = None
    collected_items = []
    encounter_chance = -EncounterChanceIncrement

    @classmethod
    def init(cls):
        Engine.calculator = Calculator((700, 5000))

    @classmethod
    def run(cls):
        screen = display.get_surface()

        dt = 0
        clock = time.Clock()
        while event.get(pygame.QUIT) == []:
            # Catch custom events
            if event.get(ATTACK_BLOCKED):
                Engine.room.monster.drop_attack()

            if event.get(DAMAGED_MONSTER):
                Engine.room.monster.hurt()

            if event.get(DAMAGED_PLAYER):
                Player.hurt()
                Engine.calculator.set_shake(500, 5)
                Engine.room.monster.drop_attack()

            missed = event.get(MISSED)
            for m in missed:
                Engine.calculator.set_shake(m.timer, 5)


            if event.get(PLAYER_DEFEATED):
                # Throw the player at the cemetery when they lose
                Engine.enter_room(name='church_cemetery')
                play_music('calm')
                continue

            # Handle navigation events
            navigation = event.get(NAVIGATE)
            for n in navigation:
                Engine.enter_room(name=n.destination)
                continue

            # Handle input
            keypresses = event.get(pygame.KEYDOWN)
            result = Engine.calculator.update(keypresses, dt)
            Engine.room.update(result, dt)
            Inventory.update(dt)

            # Render stuff
            Engine.room.draw(screen)
            Engine.calculator.draw(screen)
            Inventory.draw(screen)
            display.flip()

            # Delay after handling input and rendering screen seems to work better
            dt = clock.tick(FPS)

    @classmethod
    def enter_room(*_, **args):
        Engine.room = rooms[args['name']]
        Engine.room.boot()

        # Test for random encounter and add it to the room if we got an
        # encounter.
        if randint(0, 100) <= Engine.encounter_chance:
            monster = Gooze(difficulty=len(Inventory.items))
            Engine.room.set_monster(monster)

            # Let's absolutely guarantee that the player won't meet new enemies
            # for a few rooms after fighting.
            Engine.encounter_chance = -(Engine.EncounterBreak)*Engine.EncounterChanceIncrement

        else:
            # Make it more likely to encounter enemies if we're not meeting any
            Engine.encounter_chance += Engine.EncounterChanceIncrement

        screen = display.get_surface()
        old_surface = display.get_surface().copy()

        Engine.calculator.active = False
        Engine.calculator.set_drift(Engine.room.calculator_drift)
        Inventory.set_drift(Engine.room.calculator_drift)

        clock = time.Clock()
        time_elapsed = 0
        fade_length = 500
        dt = 0
        while True:
            check_quit()

            # Leave the function after it's done fading in
            dt += clock.tick(FPS)
            time_elapsed += dt
            if time_elapsed >= fade_length:
                break

            keypresses = event.get(pygame.KEYDOWN)
            Engine.calculator.update(keypresses, dt)

            alpha = max(0, int((255.0*(fade_length-time_elapsed))/fade_length))
            old_surface.set_alpha(alpha)

            Engine.room.draw(screen)
            Engine.calculator.draw(screen)
            screen.blit(old_surface, (0, 0))
            display.flip()

        Engine.calculator.active = True
