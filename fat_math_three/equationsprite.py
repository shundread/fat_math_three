# Pygame imports
import pygame
from pygame import display, draw, Surface

# Local imports
from data import fonts
from sprite import Sprite

#################
# Style classes #
#################

class EquationSpriteStyle(object):
    '''An object used for styling an equation sprite.'''
    def __init__(self, font, fg_color, bg_color, solved):
        self.font = font
        self.fg_color = fg_color
        self.bg_color = bg_color
        self.border_color = tuple([n/5 for n in bg_color])
        self.solved = solved

    def render_text(self, equation):
        return self.font.render(equation.text(self.solved), True, self.fg_color)

    def new_surface(self, size):
        # Create and empty the surface
        screen_flags = display.get_surface().get_flags()
        surface =  Surface(size)
        surface.fill((0, 0, 0))
        return surface

class RectangularStyle(EquationSpriteStyle):
    Margin = 10
    Edge = 5

    def __init__(self, font, fg_color, bg_color, solved):
        EquationSpriteStyle.__init__(self, font, fg_color, bg_color, solved)

    def render(self, equation):
        # Grab the text surface and size
        text_surface = self.render_text(equation)
        t_size = text_surface.get_size()

        # Request a canvas surface
        target_size = (2*self.Margin + t_size[0] + 1,
                       2*self.Margin + t_size[1] + 1)
        surface = self.new_surface(target_size)

        # Draw the rectangular shape and the text onto the surface
        draw.rect(surface, self.bg_color, surface.get_rect())
        draw.rect(surface, self.border_color, surface.get_rect(), self.Edge)
        surface.blit(text_surface, (self.Margin, self.Margin))
        return surface

###################
# Standard styles #
###################

class NavigationStyle(RectangularStyle):
    def __init__(self, solved):
        if solved:
            bg_color = (160, 255, 160)
        else:
            bg_color = (170, 170, 255)
        RectangularStyle.__init__(
            self, fonts['regular'], (0,0,0), bg_color, solved)

class ItemStyle(RectangularStyle):
    def __init__(self, solved):
        if solved:
            font = fonts['corpulent_shadow']
            bg_color = (160, 255, 160)
        else:
            font = fonts['corpulent']
            bg_color = (255, 255, 70)
        RectangularStyle.__init__(self, font, (0,0,0), bg_color, solved)

class AttackStyle(RectangularStyle):
    Ready = 0
    Solved = 1
    TimedOut = 2

    def __init__(self, state):
        font = fonts['squiggly']
        solved = (state != self.Ready)
        if state == self.Ready:
            fg_color = (0, 0, 0)
            bg_color = (255, 125, 125)
        elif state == self.Solved:
            fg_color = (0, 0, 0)
            bg_color = (160, 255, 160)
        else:
            fg_color = (255, 125, 125)
            bg_color = (0, 0, 0)
        RectangularStyle.__init__(self, font, fg_color, bg_color, solved)

class TargetStyle(RectangularStyle):
    def __init__(self, solved):
        if solved:
            bg_color = (160, 255, 160)
        else:
            bg_color = (170, 170, 255)
        RectangularStyle.__init__(
            self, fonts['regular'], (0,0,0), bg_color, solved)

################
# Sprite class #
################

class EquationSprite(Sprite):
    def __init__(self, position, equation, style):
        Sprite.__init__(self, position)
        self.equation = equation
        self.style = style

    def render(self):
        return self.style.render(self.equation)

    def set_style(self, new_style):
        self.style = new_style
        self.request_redraw()
