'''This class defines activators, which are intended to be used for anything
that can be activated, attacked or blocked in the game'''

from data import fonts

from equationsprite import (
    EquationSprite,
    NavigationStyle,
    RectangularStyle
)

############################
# Abstract activator class #
############################
class Activator(object):
    # Activator status options
    Ready = 0
    Solved = 1
    TimedOut = 2

    def __init__(self, equation, sprite):
        self.equation = equation
        self.sprite = sprite
        self.status = self.Ready

    def solved_by(self, solution):
        return self.equation.solution == solution

    def draw(self, surface, offset=(0, 0)):
        self.sprite.draw(surface, offset)

    def update(self, dt):
        self.sprite.update(dt)

    def solve(self):
        if self.status != self.Ready:
            return

        self.status = self.Solved
        self.solve_function()

##########################
# Main activator classes #
##########################
class ListeningActivator(Activator):
    '''An Activator that listens for a valid solution.'''
    def __init__(self, position, equation, unsolved_style, solved_style):
        sprite = EquationSprite(position, equation, unsolved_style)
        Activator.__init__(self, equation, sprite)
        self.solved_style = solved_style

    def solve_function(self):
        self.sprite.set_style(self.solved_style)
        self.solve_callback()

class RacingActivator(ListeningActivator):
    '''An Activator that gives the player a limited amount of time for a valid
    solution.'''
    def __init__(self, position, equation, time_left,
                 unsolved_style, solved_style, timeout_style):
        ListeningActivator.__init__(self, position, equation,
                                    unsolved_style, solved_style)
        self.timeout_style = timeout_style
        self.time_left = time_left


    def update(self, dt):
        Activator.update(self, dt)

        if self.status != self.Ready:
            return

        self.time_left = max(0, self.time_left - dt)
        if self.time_left == 0:
            self.status = self.TimedOut
            self.timeout_function()

    def timeout_function(self):
        self.sprite.set_style(self.timeout_style)
        self.timeout_callback()


##########################
# Test activator classes #
##########################

class TestRacer(RacingActivator):
    def __init__(self, position, equation, timeout):
        RacingActivator.__init__(self, position, equation, timeout,
            MenuStyle(False), MenuStyle(True),
            RectangularStyle(fonts['squiggly'], (0, 0, 0), (255, 170, 170),
            True))
        self.solve_callback_data = None
        self.timeout_callback_data = None

    def solve_callback(self, *args):
        print("I was solved")

    def timeout_callback(self, *args):
        print("I timed out")
