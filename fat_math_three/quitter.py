# Default imports
import sys

# Pygame imports
import pygame
from pygame import event

def check_quit():
    if pygame.event.get(pygame.QUIT):
        pygame.quit()
        sys.exit(0)
