import pygame

from pygame.event import post, Event

ATTACK_BLOCKED  = pygame.USEREVENT + 1
DAMAGED_MONSTER = pygame.USEREVENT + 2
DAMAGED_PLAYER  = pygame.USEREVENT + 3
MISSED          = pygame.USEREVENT + 4
NAVIGATE        = pygame.USEREVENT + 5
PLAYER_DEFEATED = pygame.USEREVENT + 6

CUSTOM_EVENTS = [
    ATTACK_BLOCKED,
    DAMAGED_MONSTER,
    DAMAGED_PLAYER,
    MISSED,
    NAVIGATE,
    PLAYER_DEFEATED
]

def post_attack_blocked():
    post(Event(ATTACK_BLOCKED))

def post_damaged_monster():
    post(Event(DAMAGED_MONSTER))

def post_damaged_player():
    post(Event(DAMAGED_PLAYER))

def post_missed(timer):
    post(Event(MISSED, timer=timer))

def post_navigate(destination):
    post(Event(NAVIGATE, destination=destination))

def post_player_defeated():
    post(Event(PLAYER_DEFEATED))
