'''This file provides an parametrized random equation composing utility.'''

from random import randint

class Equation(object):
    def __init__(self, equation, solution):
        self.__equation = equation
        self.solution = solution

    def text(self, solved):
        if solved:
            return self.__equation.replace('x', str(self.solution))
        return self.__equation

    def __repr__(self):
        return 'Equation("{0}", {1})'.format(self.__equation, self.solution)

    @classmethod
    def create_restricted(cls, limits, left_side, restricted):
        while True:
            e = Equation.create_random(limits, left_side)
            if e.solution not in restricted:
                return e

    @classmethod
    def create_random(cls, limits, left_side):
        # Replace visible elements (n) with random values within a range
        while left_side.count('n') > 0:
            value = str(randint(*limits))
            left_side = left_side.replace('n', value, 1)

        # Handle equations where x represents the right side
        if 'x' not in left_side:
            solution = eval(left_side)
            right_side = 'x'

        # Handle equations where x appears on the left side
        else:
            # Choose a solution for x
            solution = randint(*limits)
            solved_left = left_side.replace('x', str(solution))

            # Evaluate right_side accordingly. Let's cheat and use eval, which
            # is generally considered bad. ;)
            right_side = eval(solved_left)

        return Equation('{0}={1}'.format(left_side, right_side), solution)

    @classmethod
    def create_locked(cls):
        return Equation('LOCKED', 'NO SOLUTION')
