'''This class defines our on-screen calculator input widget. Not actually a
calculator, because it is missing any functions besides input of numbers.  : )'''

# Pygame imports
import pygame
from pygame.rect import Rect

# Local imports
from data import get_image, fonts
from sprite import Sprite


class Calculator(Sprite):
    Image = get_image('calculator')
    TextRect = Rect(30, 28, 122-32, 58-28)
    MaxValue = 9999999

    # Calculator positions
    TopRight    = (710, 110)
    TopLeft     = (90,  110)
    BottomRight = (710, 490)
    BottomLeft  = (90,  490)

    AcceptedKeys = {
        # Laid out like the numpad for no reason
        pygame.K_7 : 7, pygame.K_8 : 8, pygame.K_9 : 9,
        pygame.K_4 : 4, pygame.K_5 : 5, pygame.K_6 : 6,
        pygame.K_1 : 1, pygame.K_2 : 2, pygame.K_3 : 3,
        pygame.K_0 : 0,

        pygame.K_KP7 : 7, pygame.K_KP8 : 8, pygame.K_KP9 : 9,
        pygame.K_KP4 : 4, pygame.K_KP5 : 5, pygame.K_KP6 : 6,
        pygame.K_KP1 : 1, pygame.K_KP2 : 2, pygame.K_KP3 : 3,
        pygame.K_KP0 : 0,

        pygame.K_BACKSPACE : 'del',
        pygame.K_DELETE : 'del',
        pygame.K_RETURN : 'enter',
        pygame.K_KP_ENTER : 'enter',
        pygame.K_ESCAPE : 'clear'
    }

    def __init__(self, position):
        Sprite.__init__(self, position)
        self.current_value = 0
        self.cooldown = 0
        self.__active = True

    @property
    def active(self):
        return self.__active and self.cooldown == 0
    @active.setter
    def active(self, new_value):
        self.cooldown = 0
        self.__active = new_value

    ############
    # GRAPHICS #
    ############
    def render(self):
        # Render the text surface
        text = str(self.current_value)
        text_surface = fonts['calculator'].render(text, True, (0, 0, 0))

        # Calculate right-alignment position of text surface
        text_w, text_h = text_surface.get_size()
        text_x = Calculator.TextRect.right - text_w
        text_y = Calculator.TextRect.bottom - text_h

        # Copy the calculator image and paste the text surface on top of it
        result = Calculator.Image.copy()
        result.blit(text_surface, (text_x, text_y))
        return result

    ##################
    # EVENT HANDLING #
    ##################
    def set_shake(self, duration, intensity):
        Sprite.set_shake(self, duration, intensity)
        self.cooldown = duration

    def update(self, events, dt):
        '''Parses a list of events, handles cooldown, and may return a list of
        inputted numbers objects.'''
        Sprite.update(self, dt)

        # Handle cooldown
        if self.cooldown:
            self.cooldown = max(0, self.cooldown - dt)
            self.request_redraw()

        # Skip handling input if the calculator is disabled
        if not self.active:
            return

        old_value = self.current_value
        for event in events:
            # We shouldn't ever be passing non-KEYDOWN events to this object
            assert(event.type == pygame.KEYDOWN)

            if event.key not in Calculator.AcceptedKeys:
                continue

            value = Calculator.AcceptedKeys[event.key]
            if type(value) == int:
                next = self.current_value * 10 + value
                if next <= Calculator.MaxValue:
                    self.current_value = next
            elif value == 'del':
                self.current_value /= 10
            elif value == 'enter':
                result = self.current_value
                self.current_value = 0
                self.request_redraw()
                return result
            elif value == 'clear':
                self.current_value = 0
            else:
                raise Exception("How did I even get here?!")

        if self.current_value != old_value:
            self.request_redraw()
