from data import get_image
from gameevents import post_player_defeated
from sprite import Sprite

class HeartSprite(Sprite):
    def render(self):
        return get_image('heart')

class Player(object):
    hearts = []
    broken_hearts = []

    @classmethod
    def init(cls):
        cls.hearts = []
        cls.broken_hearts = []
        xpos = 350
        for h in range(3):
            heart = HeartSprite((xpos, 1000))
            heart.set_drift((xpos, 550))
            cls.hearts.append(heart)
            xpos += 50

    @classmethod
    def draw(cls, surface, offset=(0, 0)):
        for h in cls.hearts + cls.broken_hearts:
            h.draw(surface, offset)

    @classmethod
    def update(cls, dt):
        for h in cls.hearts + cls.broken_hearts:
            h.update(dt)

    @classmethod
    def hurt(cls):
        h = cls.hearts.pop()
        h.set_drift((h.position[0], 1000))
        cls.broken_hearts.append(h)

        if len(cls.hearts) == 0:
            post_player_defeated()

