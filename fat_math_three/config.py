'''A configuration file for Fat Math Three.'''

class Screen:
    w = 800
    h = 600
    Size = (w, h)

    xcenter = w/2
    ycenter = h/2
    Center = (xcenter, ycenter)

FPS = 60
