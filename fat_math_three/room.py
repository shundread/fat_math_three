'''This file defines the Room object. Not a literal room, but an environment
with a background image and interactive objects that can be activated.'''

# Pygame imports
from pygame import display, draw, event, time, Surface

# Local imports

from activator import ListeningActivator
from calculator import Calculator
from config import FPS
from data import fonts, get_image, play_music
from equation import Equation
from equationsprite import ItemStyle, NavigationStyle
from gameevents import post_navigate, post_missed
from inventory import Inventory, Item, ItemActivator
from quitter import check_quit
from player import Player
from sprite import Sprite

class Room(object):
    '''This class define a room. All of the game is spent in different rooms,
    with interactive objects and exits to other rooms.'''
    MissedTimer=500

    def __init__(self, bg_image, portals, items=[], calculator_drift=Calculator.TopRight):
        self.bg_image = get_image(bg_image, 'jpg')
        self.portals = portals
        self.items = items
        self.calculator_drift = calculator_drift
        self.boot_extra = self.__do_nothing

    def draw(self, surface, offset=(0, 0)):
        surface.blit(self.bg_image, offset)

        # Don't draw navigation activators if we got a monster
        if self.monster:
            self.monster.draw(surface, offset)
            Player.draw(surface, offset)
            return

        # Draw labels and activators
        for e in self.activators + self.labels:
            e.draw(surface, offset)

        # Draw item activators
        for i in self.item_activators:
            i.draw(surface, offset)

    def update(self, result, dt):
        '''Updates all activators either by using a solution or running their
        tiers.'''

        # Don't do anything if we got a monster
        if self.monster:
            if self.monster.status == self.monster.Killed:
                self.monster = None
                play_music('calm')
            else:
                self.monster.update(result, dt)
                Player.update(dt)
                return

        if result is not None:
            missed = True
        else:
            missed = False

        for a in self.activators:
            # Check for solved_by first. Let's give the players one frame of
            # advantage over the timer. ;)
            if a.solved_by(result):
                a.solve()
                missed = False
            a.update(dt)

        # Update item activators, maybe
        for i in self.item_activators:
            if i.solved_by(result):
                i.solve()
                missed = False
            i.update(dt)

        if missed:
            post_missed(self.MissedTimer)
    

    def boot(self):
        '''Distributes whatever required navigation activators to the room, as
        well as other item accessors'''
        # Add any navigation activators and labels
        self.labels = []
        self.activators = []
        used_solutions = []
        self.monster = None
        for p in self.portals:
            if not p.requires:
                locked = False
            if p.requires:
                locked = True
                for i in Inventory.items:
                    if i.name == p.requires:
                        locked = False
                        break
            if locked:
                equation = Equation.create_locked()
            else:
                equation = p.make_equation(used_solutions)
                used_solutions.append(equation.solution)
            self.activators.append(NavigationActivator(p, equation))
            self.labels.append(PortalLabel(p))

        self.item_activators = []
        for i in self.items:
            # Skip already collected items
            if i in Inventory.items:
                continue
            equation = i.make_equation(used_solutions)
            used_solutions.append(equation.solution)
            self.item_activators.append(ItemActivator(i, equation))

        # Call boot extra to handle any need to add other activators based on
        # random or planned monster encounters, player inventory tests.
        self.boot_extra()

    def set_monster(self, monster):
        play_music('combat')
        Player.init()
        self.monster = monster

    def __do_nothing(self):
        '''Default boot extra function. Does nothing.'''
        pass

class PortalLabel(Sprite):
    '''This defines a label that hovers above a portal's equation.'''
    # Duplicates some functionality. Bad design.
    Margin = 8
    Edge = 3

    def __init__(self, portal):
        (x, y) = portal.position
        pos = (x, y-60)
        Sprite.__init__(self, pos)
        self.portal = portal

    def new_surface(self, size):
        # Create and empty the surface
        screen_flags = display.get_surface().get_flags()
        surface =  Surface(size)
        surface.fill((180, 180, 180))
        return surface

    def render(self):
        # Grab the text surface and size
        text_surface = fonts['regular'].render(self.portal.label, True, (0,0,0))
        t_size = text_surface.get_size()

        # Request a canvas surface
        target_size = (2*self.Margin + t_size[0] + 1,
                       2*self.Margin + t_size[1] + 1)
        surface = self.new_surface(target_size)

        # Draw the rectangular shape and the text onto the surface
        draw.rect(surface, (18,18,18), surface.get_rect(), self.Edge)
        surface.blit(text_surface, (self.Margin, self.Margin))
        return surface

class Portal(object):
    def __init__(self, difficulty, limits, label, destination, position, requires=None):
        self.difficulty = difficulty
        self.limits = limits
        self.label = label
        self.destination = destination
        self.position = position
        self.requires = requires


    def make_equation(self, used_solutions):
        if self.difficulty == 4:
            formula = 'n*x'
        elif self.difficulty == 3:
            formula = 'n*n'
        elif self.difficulty == 2:
            formula = 'n-x'
        elif self.difficulty == 1:
            formula = 'n+x'
        else:
            formula = 'n+n'
        # Default: Difficulty 0
        return Equation.create_restricted(self.limits, formula, used_solutions)


class NavigationActivator(ListeningActivator):
    def __init__(self, portal, equation):
        ListeningActivator.__init__(self, portal.position, equation,
            NavigationStyle(False), NavigationStyle(True))
        self.destination = portal.destination

    def solve_callback(self):
        post_navigate(self.destination)


rooms = {}

def init_rooms():
    rooms['intro'] = Room('intro', [
        Portal(0, (1, 5), 'Solve for x to start the game', 'plains1', (400, 300))
    ])

    ##########
    # Plains #
    ##########
    rooms['plains1'] = Room('plains1', [
        Portal(0, (1, 5), 'You see more plains to the north', 'plains2', (400, 100))
    ])

    rooms['plains2'] = Room('plains2', [
        Portal(0, (1, 5), 'You see more plains to the south', 'plains1', (400, 550)),
        Portal(0, (1, 6), 'You see a road to the north', 'road1', (400, 100)),
    ])

    #########
    # Roads #
    #########
    rooms['road1'] = Room('road1', [
        Portal(0, (1, 6), 'You see some plains to the south', 'plains2', (400, 550)),
        Portal(0, (1, 7), 'You see more road to the north', 'road2', (400, 100)),
        Portal(2, (1, 5), 'You see a house to the east', 'house_entrance', (600, 300)),
    ], calculator_drift=Calculator.TopLeft)

    rooms['road2'] = Room('road2', [
        Portal(0, (1, 8), 'You see more road to the south', 'road1', (400, 550)),
        Portal(0, (1, 9), 'You see more road to the north', 'road3', (400, 100)),
        Portal(1, (1, 5), 'You see a school to the west', 'school_entrance', (200, 300)),
    ])

    rooms['road3'] = Room('road3', [
        Portal(0, (1, 9), 'You see more road to the south', 'road2', (400, 550)),
        Portal(0, (1, 10), 'You see more road to the north', 'road4', (400, 100)),
        Portal(4, (1, 5), 'You see a laboratory to the east', 'lab_entrance', (600, 300)),
    ], calculator_drift=Calculator.TopLeft)

    rooms['road4'] = Room('road3', [
        Portal(0, (1, 10), 'You see more road to the south', 'road3', (400, 550)),
        Portal(3, (1, 5), 'You see a church to the north', 'church_entrance', (400, 100)),
    ])

    #########
    # House #
    #########
    rooms['house_entrance'] = Room('house_entrance', [
        Portal(0, (1, 5), 'You see a road to the west', 'road1', (200, 300)),
        Portal(2, (1, 7), 'You can enter the kitchen from here', 'house_kitchen', (550, 200), requires='key_house')
    ], calculator_drift=Calculator.BottomRight)

    rooms['house_kitchen'] = Room('house_kitchen', [
        Portal(2, (1, 7), 'You can leave the house from here', 'house_entrance', (400, 100)),
        Portal(2, (1, 9), 'You can enter the bedroom from here', 'house_bedroom', (230, 550))
    ])

    rooms['house_bedroom'] = Room('house_bedroom', [
        Portal(2, (1, 11), 'You can return to the kitchen from here', 'house_kitchen', (400, 550))
    ], items = [
        Item(2, (1, 10), 'key_church', (400, 200))
    ])

    ##########
    # School #
    ##########
    rooms['school_entrance'] = Room('school_entrance', [
        Portal(0, (1, 5), 'You see a road to the east', 'road2', (600, 300)),
        Portal(1, (1, 5), 'You can enter the lobby from here', 'school_lobby', (300, 500))
    ], calculator_drift=Calculator.TopLeft)

    rooms['school_lobby'] = Room('school_lobby', [
        Portal(1, (1, 7), 'You can exit the school from here', 'school_entrance', (500, 100)),
        Portal(1, (1, 9), 'You can enter the classroom from here', 'school_classroom', (250, 550))
    ], calculator_drift=Calculator.TopLeft)

    rooms['school_classroom'] = Room('school_classroom', [
        Portal(1, (1, 11), 'You can return to the lobby from here', 'school_lobby', (400, 550))
    ], items = [
        Item(1, (1, 10), 'key_house', (400, 200))
    ], calculator_drift=Calculator.TopLeft)

    ##############
    # Laboratory #
    ##############
    rooms['lab_entrance'] = Room('lab_entrance', [
        Portal(0, (1, 5), 'You see a road to the west', 'road3', (100, 300)),
        Portal(4, (2, 8), 'You can enter the lab from here', 'lab_corridor', (400, 100), requires='key_lab')
    ])

    rooms['lab_corridor'] = Room('lab_corridor', [
        Portal(4, (2, 12), 'You can leave the lab from here', 'lab_entrance', (400, 550)),
        Portal(4, (2, 12), 'You can end the game here', 'the_end', (400, 100))
    ])

    # Placeholder endign room
    rooms['the_end'] = Room('the_end', [])

    ##########
    # Church #
    ##########
    rooms['church_entrance'] = Room('church_entrance', [
        Portal(0, (1, 5), 'You can see a road to the south', 'road4', (400, 550)),
        Portal(3, (2, 8), 'There is a cemetery to the west', 'church_cemetery', (200, 100)),
        Portal(3, (2, 10), 'There is a door to the east', 'church_doorway', (600, 100), requires='key_church') #XXX Wrong place!
    ], calculator_drift=Calculator.BottomRight)

    rooms['church_cemetery'] = Room('church_cemetery', [
        Portal(3, (2, 8), 'Nothing to do here', 'church_entrance', (600, 300))
    ], calculator_drift=Calculator.TopLeft)

    rooms['church_doorway'] = Room('church_doorway', [
        Portal(3, (2, 10), 'Leave church', 'church_entrance', (400, 550))
    ], items = [
        Item(3, (2, 10), 'key_lab', (400, 200))
    ])
