# Pygame imports
import pygame

from pygame import display, event, time

# Local imports
from config import Screen, FPS
from data import fonts, get_image
from quitter import check_quit

def play_intro():
    logo = get_image('shundread')
    (logo_w, logo_h) = logo.get_size()
    logo_pos = (Screen.xcenter - (logo_w/2), Screen.ycenter - logo_h)

    title = 'SHUNDREAD IS ON FIRE!!1'
    text = fonts['regular'].render(title, True, (255,255,255), (0, 0, 0))
    (text_w, text_h) = text.get_size()
    text_pos = (Screen.xcenter - (text_w/2), logo_pos[1] + logo_h + 10)

    screen = display.get_surface()

    clock = time.Clock()
    time_elapsed = 0
    fade_in_length = 2000
    intro_length = 4000
    while True:
        check_quit()

        # Allow skipping intro
        if event.get(pygame.KEYDOWN):
            return

        # Fade in
        alpha = min(255, int(255.0 * time_elapsed / fade_in_length))
        logo.set_alpha(alpha)
        text.set_alpha(alpha)

        screen.fill((0, 0, 0))
        screen.blit(logo, logo_pos)
        screen.blit(text, text_pos)
        display.flip()

        # Leave the intro after it's done displaying
        time_elapsed += clock.tick(FPS)
        if time_elapsed >= intro_length:
            return

