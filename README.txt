Fat Math Three
===============

Entry in PyWeek #21  <http://www.pyweek.org/21/>
URL: https://www.pyweek.org/e/fat_math_three/
Team: shundread
Members: Thiago Chaves de Oliveira Horta <shundread@gmail.com>
License: see LICENSE.txt


Running the Game
----------------

On Windows or Mac OS X, locate the "run_game.pyw" file and double-click it.

Othewise open a terminal / console and "cd" to the game directory and run:

  python run_game.py


How to Play the Game
--------------------

A game about calculating your way through challenges and retrieving The Father Mat, which can bring the timeline back into balance.

Use your keyboard's number keys and the enter key to perform actions in the game.

Development notes 
-----------------

Creating a source distribution with:

   python setup.py sdist

You may also generate Windows executables and OS X applications:

   python setup.py py2exe
   python setup.py py2app

Upload files to PyWeek with:

   python pyweek_upload.py

Upload to the Python Package Index with:

   python setup.py register
   python setup.py sdist upload
